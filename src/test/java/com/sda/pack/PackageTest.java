package com.sda.pack;

import com.sda.info.Address;
import com.sda.info.Person;
import model.RandomObject;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;

@RunWith(MockitoJUnitRunner.class)
public class PackageTest {
    private Package<RandomObject> p;

    @Mock
    private Address a;

    @Rule
    public ExpectedException e = ExpectedException.none();


    @Before
    public void testSetup(){
        p = new Package<>();
    }


    @Test
    public void setItem() {
        RandomObject ro = new RandomObject();
        // testing fluent interface
        assertThat(p.setItem(ro)).isEqualToComparingFieldByFieldRecursively(p);
        // testing setItem
        assertThat(p.getContent()).isNotNull().isInstanceOf(RandomObject.class).isEqualToComparingFieldByFieldRecursively(ro);
    }

    @Test
    public void setReciever() {
        Person r = new Person("John Doe", a);
        // testing fluent interface
        assertThat(p.setReciever(r)).isEqualToComparingFieldByFieldRecursively(p);
        // testing setReciever
        assertThat(p.getReciever()).isNotNull().isEqualToComparingFieldByFieldRecursively(r);
    }

    @Test
    public void setSender() {
        Person s = new Person("Jane Doe", a);
        // testing fluent interface
        assertThat(p.setSender(s)).isEqualToComparingFieldByFieldRecursively(p);
        // testing setSender
        assertThat(p.getSender()).isNotNull().isEqualToComparingFieldByFieldRecursively(s);
        assertThat(p.getAddress()).isNotNull().isEqualToComparingFieldByFieldRecursively(s.getAddress());
    }

    @Test
    public void setAddress() {
        // testing fluent interface
        assertThat(p.setAddress(a)).isEqualToComparingFieldByFieldRecursively(p);
        // testing setAddress
        assertThat(p.getAddress()).isNotNull().isEqualToComparingFieldByFieldRecursively(a);
    }

    @Test
    public void setStatus(){
        // testing fluent interface
        assertThat(p.setStatus(Status.ACCEPTED)).isEqualToComparingFieldByFieldRecursively(p);
        // testing setStatus
        assertThat(p.getStatus()).isNotNull().isEqualToComparingFieldByFieldRecursively(Status.ACCEPTED);
    }

    @Test
    public void create() throws PackageException {
        RandomObject ro = new RandomObject();
        Address a2 = mock(Address.class);
        Person s = new Person("John Doe", a);
        Person r = new Person("Jane Doe", a2);
        p.setItem(ro).setSender(s).setReciever(r);
        // id should be null before creation
        assertThat(p.getUuid()).isNull();
        // package creation
        assertThat(p.create()).isNotNull().isEqualToComparingFieldByFieldRecursively(p);
        assertThat(p.getUuid()).isNotNull();
        assertThat(p.getStatus()).isEqualToComparingFieldByFieldRecursively(Status.ACCEPTED);
    }

    @Test
    public void createExceptionNoItem() throws PackageException {
        e.expect(PackageException.class);
        Address a2 = mock(Address.class);
        Person s = new Person("John Doe", a);
        Person r = new Person("Jane Doe", a2);
        p.setSender(s).setReciever(r);
        p.create();
    }

    @Test
    public void createExceptionNoSenderAndAddress() throws PackageException {
        e.expect(PackageException.class);
        RandomObject ro = new RandomObject();
        Address a2 = mock(Address.class);
        Person r = new Person("Jane Doe", a2);
        p.setItem(ro).setReciever(r);
        p.create();
    }

    @Test
    public void createExceptionNoReciever() throws PackageException {
        e.expect(PackageException.class);
        RandomObject ro = new RandomObject();
        Person s = new Person("John Doe", a);
        p.setItem(ro).setSender(s);
        p.create();
    }

}