package com.sda.info;

public enum Country {
    PL("Poland"),
    US("United States"),
    UK("United Kingdom"),
    FR("France");

    private String countryName;

    Country(String countryName){
        this.countryName = countryName;
    }
}
