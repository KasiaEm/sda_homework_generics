package com.sda.info;

public class Address {
    private Country country;
    private String state;
    private String city;
    private String streetAddress;

    public Address(Country country, String state, String city, String streetAddress) {
        this.country = country;
        this.state = state;
        this.city = city;
        this.streetAddress = streetAddress;
    }

    public Country getCountry() {
        return country;
    }

    public String getState() {
        return state;
    }

    public String getCity() {
        return city;
    }

    public String getStreetAddress() {
        return streetAddress;
    }
}
