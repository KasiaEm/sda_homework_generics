package com.sda.info;

public class Person {
    private String fullName;
    private Address address;

    public Person(String fullName, Address address) {
        this.fullName = fullName;
        this.address = address;
    }

    public String getFullName() {
        return fullName;
    }

    public Address getAddress() {
        return address;
    }
}
