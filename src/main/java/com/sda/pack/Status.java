package com.sda.pack;

public enum Status {
    ACCEPTED(0), SENT(1), WAITING (2), RECIEVED(3);

    private int statusNr;

    Status(int statusNr){
        this.statusNr = statusNr;
    }

    public int getStatusNr() {
        return statusNr;
    }
}
