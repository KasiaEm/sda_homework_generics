package com.sda.pack;

import com.sda.info.Address;
import com.sda.info.Person;

import java.util.UUID;

public class Package<T> {
    private T content;
    private String uuid;
    private Person reciever;
    private Person sender;
    private Status status;
    private Address address;

    public Package<T> setItem(T content){
        this.content = content;
        return this;
    }

    public Package<T> setReciever(Person reciever){
        this.reciever = reciever;
        return this;
    }
    public Package setSender(Person sender){
        this.sender = sender;
        this.address = sender.getAddress();
        return this;
    }

    public Package<T> setAddress(Address address) {
        this.address = address;
        return this;
    }

    public Package<T> create() throws PackageException{
        if(content==null||reciever==null||sender==null||address==null){
            throw new PackageException("Info missing. Package cannot be sent.");
        }
        uuid = UUID.randomUUID().toString();
        status = Status.ACCEPTED;
        return this;
    }

    public Package<T> setStatus(Status status) {
        this.status = status;
        return this;
    }

    public T getContent() {
        return content;
    }

    public String getUuid() {
        return uuid;
    }

    public Person getReciever() {
        return reciever;
    }

    public Person getSender() {
        return sender;
    }

    public Status getStatus() {
        return status;
    }

    public Address getAddress() {
        return address;
    }
}
