package com.sda.pack;

public class PackageException extends Exception{
    public PackageException(String message){
        super(message);
    }
}
